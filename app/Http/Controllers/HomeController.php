<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Redirect;
use App\Mail\Recomendation;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class HomeController extends Controller
{
    public function customers(Request $request) {
        $customers = Customer::all();
        return view('customers', compact('customers'));
    }

    public function customers_store(Request $request) {
        $request->validateWithBag('customerCreation', [
            'name' => ['required', 'max:50'],
            'owner_name' => ['required', 'max:50'],
            'email' => ['required', 'email'],
            'location'=> ['required']
        ]);

        $customer = new Customer;
        $customer->name = $request->name;
        $customer->owner_name = $request->owner_name;
        $customer->email = $request->email;
        $customer->location = $request->location;
        $customer->save();

        return Redirect::route('customers');
    }

    public function customers_sendmail(Request $request, $id) {
        $customer = Customer::findOrFail($id);
        Mail::to($customer->email)->send(new Recomendation($customer->name, $customer->owner_name, Crypt::encryptString($id)));
        return Redirect::route('customers');
    }
}
