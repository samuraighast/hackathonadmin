<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class Recomendation extends Mailable
{
    use Queueable, SerializesModels;

    public $business_name;
    public $owner_name;
    public $hash;

    /**
     * Create a new message instance.
     */
    public function __construct($business_name, $owner_name, $hash)
    {
        $this->business_name = $business_name;
        $this->owner_name = $owner_name;
        $this->hash = $hash;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address(env('MAIL_USERNAME'), env('APP_NAME')),
            subject: 'Unlock New Opportunities: Transition to Card Payments for '.$this->business_name,
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'mail.recomendation',
            with: ['business_name' => $this->business_name, 'owner_name' => $this->owner_name, 'hash' => $this->hash],
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
