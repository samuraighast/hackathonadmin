<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Customers') }}
            </h2>
            <x-primary-button x-data=""
                x-on:click.prevent="$dispatch('open-modal', 'create-customer')">{{ __('Create Customer') }}</x-primary-button>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="flex flex-col">
                        <div class="-m-1.5 overflow-x-auto">
                            <div class="p-1.5 min-w-full inline-block align-middle">
                                <div class="overflow-hidden">
                                    <table class="min-w-full divide-y divide-gray-200">
                                        <thead>
                                            <tr>
                                                <th scope="col"
                                                    class="px-6 py-3 text-start text-xs font-medium text-gray-500 uppercase">
                                                    Name</th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-start text-xs font-medium text-gray-500 uppercase">
                                                    Owner Name</th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-start text-xs font-medium text-gray-500 uppercase">
                                                    Email Address</th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-start text-xs font-medium text-gray-500 uppercase">
                                                    Location</th>
                                                <th scope="col"
                                                    class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase w-40">
                                                    Action</th>
                                            </tr>
                                        </thead>
                                        <tbody class="divide-y divide-gray-200">
                                            @foreach ($customers as $customer)
                                                <tr>
                                                    <td
                                                        class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800">
                                                        {{ $customer->name }}</td>
                                                    <td
                                                        class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800">
                                                        {{ $customer->owner_name }}</td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">
                                                        {{ $customer->email }}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">
                                                        {{ $customer->location }}</td>
                                                    <td
                                                        class="px-6 py-4 whitespace-nowrap text-end text-sm font-medium">
                                                        <form method="post"
                                                            action="{{ route('customers.sendmail', ['id' => $customer->id]) }}">
                                                            @csrf
                                                            <x-secondary-button type="submit">
                                                                {{ __('Send Mail') }}
                                                            </x-secondary-button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-modal name="create-customer" :show="$errors->customerCreation->isNotEmpty()">
        <form method="post" action="{{ route('customers.store') }}" class="p-6">
            @csrf

            <h2 class="text-lg font-medium text-gray-900">
                {{ __('Please provide some information about the customer') }}
            </h2>

            <div class="mt-6">
                <x-input-label for="name" value="{{ __('Name') }}" class="sr-only" />

                <x-text-input id="name" name="name" type="text" class="mt-1 block w-3/4"
                    value="{{ old('name') }}" placeholder="{{ __('Name') }}" />

                <x-input-error :messages="$errors->customerCreation->get('name')" class="mt-2" />
            </div>

            <div class="mt-6">
                <x-input-label for="owner_name" value="{{ __('Owner Name') }}" class="sr-only" />

                <x-text-input id="owner_name" name="owner_name" type="text" class="mt-1 block w-3/4"
                    value="{{ old('owner_name') }}" placeholder="{{ __('Owner Name') }}" />

                <x-input-error :messages="$errors->customerCreation->get('owner_name')" class="mt-2" />
            </div>

            <div class="mt-6">
                <x-input-label for="email" value="{{ __('Email') }}" class="sr-only" />

                <x-text-input id="email" name="email" type="text" class="mt-1 block w-3/4"
                    value="{{ old('email') }}" placeholder="{{ __('Email') }}" />

                <x-input-error :messages="$errors->customerCreation->get('email')" class="mt-2" />
            </div>

            <div class="mt-6">
                <x-input-label for="location" value="{{ __('Location (latitude,longitude)') }}" class="sr-only" />

                <x-text-input id="location" name="location" type="text" class="mt-1 block w-3/4"
                    value="{{ old('location') }}" placeholder="{{ __('Location (latitude,longitude)') }}" />

                <x-input-error :messages="$errors->customerCreation->get('location')" class="mt-2" />
            </div>

            <div class="mt-6 flex justify-end">
                <x-secondary-button x-on:click="$dispatch('close')">
                    {{ __('Cancel') }}
                </x-secondary-button>

                <x-primary-button class="ms-3">
                    {{ __('Save') }}
                </x-primary-button>
            </div>
        </form>
    </x-modal>
</x-app-layout>
