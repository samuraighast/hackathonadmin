<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div id='map' style='width: 100%; height: 70vh;'></div>
                    <script>
                        mapboxgl.accessToken =
                            'pk.eyJ1Ijoib3RhYmVrMDEyMCIsImEiOiJja3FmNDlta2wwZTVoMnZxdWxuOGRvMTZ2In0.Xj2SoVbfTSh7USRf_BTArQ';
                        const map = new mapboxgl.Map({
                            container: 'map',
                            style: 'mapbox://styles/mapbox/streets-v12',
                            center: [13.038054557755336, 47.80750159071447],
                            zoom: 12
                        });
                        const marker = new mapboxgl.Marker({
                                color: "#475dcd",
                                draggable: false
                            })
                            .setLngLat([13.038054557755336, 47.80750159071447])
                            .addTo(map);
                    </script>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
