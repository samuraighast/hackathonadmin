<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <title>Recommendation Email</title>
</head>

<body style="font-family: 'Poppins', sans-serif; background-color:#2ecc7145; padding: 10px 0;">

    <div
        style="max-width: 600px; margin: 0 auto; padding: 20px; background-color:#ffffff; border-radius: 10px; box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;">

        <h2 style="color: #333;">Unlock New Opportunities: Transition to Card Payments for {{ $business_name ?? '-' }}
        </h2>

        <p>Dear {{ $owner_name ?? '-' }},</p>

        <p>
            I hope this message finds you well. My name is Otabek Sobirov, and I represent LocaleVista,
            a
            platform dedicated to supporting local businesses in maximizing their potential.
        </p>

        <p>
            We recently conducted an in-depth analysis of businesses in our region, and your establishment,
            {{ $business_name ?? '-' }}, stood out as a prominent and valuable contributor to the community. However, we
            noticed that your
            business currently accepts only cash.
        </p>

        <p>
            In light of the evolving preferences of customers, especially tourists, we would like to recommend the
            adoption of card payments at {{ $business_name ?? '-' }}. Our analysis suggests that embracing card
            transactions could
            bring about significant benefits for your business, including:
        </p>

        <ul>
            <li><strong>Expanded Customer Base:</strong> Tourists, in particular, prefer the convenience of card
                payments. By accepting cards, you can attract a broader range of customers and enhance their overall
                experience.</li>
            <li><strong>Increased Revenue:</strong> Card payments tend to lead to higher transaction amounts,
                contributing to increased revenue for your business.</li>
            <li><strong>Competitive Edge:</strong> Many of your peers in the industry have already adopted card
                payments, providing them with a competitive edge. Offering this convenience can help you stay ahead in
                the market.</li>
        </ul>

        <p>
            To provide you with more insights into this recommendation, we have prepared a detailed analysis on our
            website. You can access the report by clicking on the following link:
            <br><a href="{{ route('analysis', ['hash' => $hash ?? '-']) }}" style="color: #007BFF; text-decoration: none;">{{ route('analysis', ['hash' => $hash ?? '-']) }}</a> <br>The
            report covers key trends, benefits, and success stories related to businesses that have transitioned from
            cash-only to accepting card payments.
        </p>

        <p>
            We understand that this transition may seem challenging, but we are here to support you every step of the
            way. Our team can provide assistance in setting up card processing systems, offer guidance on promotional
            strategies, and address any concerns you may have.
        </p>

        <p>
            Feel free to reach out to us with any questions or to discuss this recommendation further. We believe that
            by embracing card payments, {{ $business_name ?? '-' }} can unlock new opportunities for growth and better
            serve the
            needs of your customers.
        </p>

        <p>Thank you for your time, and we look forward to the possibility of working together to elevate the success
            of {{ $business_name ?? '-' }}.</p>

        <p>Best Regards,</p>
        <p>Otabek Sobirov<br>
            Customer Manager<br>
            +4368184800000<br>
        </p>

    </div>

</body>

</html>
