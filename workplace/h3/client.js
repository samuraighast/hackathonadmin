const grpc = require("@grpc/grpc-js");
const PROTO_PATH = "./h3.proto";
var protoLoader = require("@grpc/proto-loader");

const options = {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
};

var packageDefinition = protoLoader.loadSync(PROTO_PATH, options);

const H3Service = grpc.loadPackageDefinition(packageDefinition).H3Service;

const client = new H3Service(
    "localhost:50051",
    grpc.credentials.createInsecure()
);

const latLngToCellRequest = {
    latitude: 37.3615593,
    longitude: -122.0553238,
    resolution: 7,
};

client.LatLngToCell(latLngToCellRequest, (error, response) => {
    if (!error) {
        const h3Index = response.h3Index;
        console.log('LatLngToCell - H3 Index:', h3Index);

        // Example usage of CellToBoundary
        const cellToBoundaryRequest = { h3Index };

        client.CellToBoundary(cellToBoundaryRequest, (boundaryError, boundaryResponse) => {
            if (!boundaryError) {
                const boundaryPoints = boundaryResponse.points;
                console.log('CellToBoundary - Boundary Points:', boundaryPoints);
            } else {
                console.error(boundaryError);
            }
        });
    } else {
        console.error(error);
    }
});