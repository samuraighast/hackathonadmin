const grpc = require("@grpc/grpc-js");
const PROTO_PATH = "./h3.proto";
var protoLoader = require("@grpc/proto-loader");

const h3 = require("h3-js");

const options = {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
};
var packageDefinition = protoLoader.loadSync(PROTO_PATH, options);
const h3Proto = grpc.loadPackageDefinition(packageDefinition);

const server = new grpc.Server();

server.addService(h3Proto.H3Service.service, {
    LatLngToCell: (call, callback) => {
        const { latitude, longitude, resolution } = call.request;
        const h3Index = h3.latLngToCell(latitude, longitude, resolution);
        callback(null, { h3Index });
    },

    CellToBoundary: (call, callback) => {
        const { h3Index } = call.request;
        const boundaryPoints = h3.cellToBoundary(h3Index);

        const response = {
            points: boundaryPoints.map(point => ({ latitude: point[0], longitude: point[1] })),
        };

        callback(null, response);
    },
});

server.bindAsync(
    "127.0.0.1:50051",
    grpc.ServerCredentials.createInsecure(),
    (error, port) => {
        console.log("Server running at http://127.0.0.1:50051");
        server.start();
    }
);